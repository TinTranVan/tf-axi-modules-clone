/************************************************
* Instace outputs
***********************************************/
output "ci_db_id" {
  value = "${cloudstack_instance.ci_db.id}"
}

output "ci_db_name" {
  value = "${cloudstack_instance.ci_db.name}"
}