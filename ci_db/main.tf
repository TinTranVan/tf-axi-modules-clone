/************************************************
* Instance construction as requires custom block
* devices to be made
***********************************************/
resource "cloudstack_instance" "ci_db" {
  name                = "${format("tf-%v-%v-%v-%v", var.instance_name, var.service, var.client, var.env)}"
  service_offering    = "${var.service_offering}"
  template            = "${var.instance_template}"
  network_id          = "${var.network_id}"
  zone                = "${var.cs_zone}"
  root_disk_size      = "${var.root_disk_size}"
  user_data           = "${data.template_cloudinit_config.config.rendered}"
  keypair             = "${var.ssh_keypair}"
  expunge             = true
}

# WARNING: Volumes will be deleted automatically with the instance????
# Data
resource "cloudstack_disk" "dbdata" {
  name                = "${format("%v-PGDATA", cloudstack_instance.ci_db.name)}"
  attach              = "true"
  disk_offering       = "${var.dbdata_disk_offerings}"
  virtual_machine_id  = "${cloudstack_instance.ci_db.id}"
  zone                = "${var.cs_zone}"
}

# Index
resource "cloudstack_disk" "dbindex" {
  depends_on          = ["cloudstack_disk.dbdata"]
  name                = "${format("%v-PGINDEX", cloudstack_instance.ci_db.name)}"
  attach              = "true"
  disk_offering       = "${var.dbindex_disk_offerings}"
  virtual_machine_id  = "${cloudstack_instance.ci_db.id}"
  zone                = "${var.cs_zone}"
}

# Log and temporaty files
resource "cloudstack_disk" "dblogs" {
  depends_on          = ["cloudstack_disk.dbindex"]
  name                = "${format("%v-PGLOGS", cloudstack_instance.ci_db.name)}"
  attach              = "true"
  disk_offering       = "${var.dblog_disk_offerings}"
  virtual_machine_id  = "${cloudstack_instance.ci_db.id}"
  zone                = "${var.cs_zone}"
}