variable "filename" {
  default = "cloud-config.cfg"
}

data "template_file" "user_data" {
  template = "${file("${path.module}/../templates/userdata.tpl")}"

  vars {
    instance_name = "${var.instance_name}"
    service = "${var.service}"
    client = "${var.client}"
    env = "${var.env}"
    prov-reg = "${var.prov-reg}"
    domain = "${var.domain}"
    salt_minion_version = "${var.salt_minion_version}"
    salt_master_fqdn = "${var.salt_master_fqdn}"
  }
}

data "template_cloudinit_config" "config"{
  gzip = false
  base64_encode = false

  part {
    filename = "${var.filename}"
    content_type = "text/cloud-config"
    content = "${data.template_file.user_data.rendered}"
  }
}