/************************************************
* General info
***********************************************/

variable "cs_zone"     {
  description = "Cloudstack zone to be used"
}

/************************************************
* Instance info
***********************************************/

variable "service_offering" {
  description = "Instance service offering to use"
  default = "2vCPU-4GB-SSD-STD-SF"
}

variable "instance_template" {
  description = "Instance template to use"
  default = "AXI-CentOS-7-x86_64-Minimal_V2"
}

variable "root_disk_size" {
  description = "Set the root disk size in GB"
}

variable "dbdata_disk_offerings" {
  description = "Data disk offering"
  default = "50GB-SSD-STD-SF"
}

variable "dbindex_disk_offerings" {
  description = "Index disk offering"
  default = "50GB-SSD-STD-SF"
}

variable "dblog_disk_offerings" {
  description = "Logs and temp disk offering"
  default = "50GB-SSD-STD-SF"
}

variable "ssh_keypair" {
  description = "The name of the SSH key pair to access this instance"
}

variable "instance_name" {
  description = "PostgreSQL instance name"
}

variable "network_id" {
  description = "Cloudstack hosting network id for instance"
}

variable "domain_name_servers" {
  description = "String of comma seperated domain name servers to use in DHCP option sets"
  default = "8.8.8.8,8.8.4.4"
}


/************************************************
* Userdata variables
***********************************************/
variable "service" {
  description = "Most granular of taxonomy. e.g. `promise`, `dataproc`, `cassandra`, `mgmt`, etc"
}

variable "client" {
  description = "A business entity/client of devops. e.g. `mobiliar`, `suva`, `axi`, `ops`"
}

variable "env" {
  description = "Short for environment. e.g. `dev`, `test`, `stg`, `prod`"
}

variable "prov-reg" {
  description = "Region and cloud provider identifier e.g `s1s` or `s2s` for Swiss1/Swiss2 of SSC"
}

variable "domain" {
  description = "The domain used for the instance"
}

variable "salt_minion_version" {
  description = "Salt minion version to install"
}

variable "salt_master_fqdn" {
  description = "Salt master FQDN or IP"
}
