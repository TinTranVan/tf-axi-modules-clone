#cloud-config
manage_etc_hosts: true
locale: "en_US.UTF-8"
fqdn: ${instance_name}-${client}-${service}-${env}-${prov-reg}.${domain}
system_info:
default_user:
name: cloud-user
chpasswd:
list: |
root:s3cr3t
expire: False
disable_root: True
manage-resolv-conf: true
resolv_conf:
nameservers: ['8.8.4.4', '8.8.8.8']
options:
rotate: true
timeout: 1
#package_update: true
#package_upgrade: true
yum_repos:
saltrepo:
baseurl: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/${salt_minion_version}
enabled: true
failovermethod: priority
gpgcheck: true
gpgkey: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/${salt_minion_version}/SALTSTACK-GPG-KEY.pub
name: Salt
packages:
- salt-minion
runcmd:
- 'cp /etc/skel/.bashrc /root/.bashrc'
- 'sed -i -e "/^#master: salt/s/^.*\$/master: ${salt_master_fqdn}/" /etc/salt/minion'
- 'systemctl enable salt-minion.service'
- 'systemctl restart salt-minion.service'
- 'mkdir -p /var/ssh/aavn-ict/'
write_files:
- content: |
alias ls='ls --color=auto'
alias grep='grep --color=auto'
N="\[\e[0m\]"
R="\[\e[1;31m\]"
G="\[\033[32m\]"
if [[ $EUID == 0 ]] ; then
PS1="$R\u@\h :[ $N\w$R ]:# $N"
else
PS1="$G\u@\h :[ $N\w$G ]:\$ $N"
fi
unset R G N
#
path: /etc/skel/.bashrc
owner: root:root
permissions: '0644'
- content: |
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAzcGyrSSzfpVywHFHS3xVT9CgvCh+jwiR9jitJOHJ9/JLW6YO
8Wf58uJ2JJolY6rl8Jkl8gfH3KZjLC2xVfgDBzJkH6p8EHXyh3uNcwiO64PzcbHc
yJ/5czUqFZmrYbMd8Aq2kvPr8q8weFVaM8HDEOw3hkQbT14zLTHIFwrRqY3MNN+F
dLg8tMLwa6P180U0vCDPevxSjjAnLfqr3lq4pNQMMsTZdRoxSoGIP6BL+Ydn6fGm
pRbqjzSZDIpbRW1tZfq5QE0jKHEpGnZxMIXXrO/dm+syAI5lK8jLc4JidHx8iAL+
vgC+sjUlBlhI74G8p6u9dFiXDCvMIO9HczUI8QIDAQABAoIBAAVj/r9kh8NYPQJn
tVt37XBC4gMr+Gzc554QzBB8TeNOGMVU621NsRG49ylsVHBxCMct6/2UUJsqTMza
gZvj/IhSvKbLu6ZewLTl+sW5wqzO7zDaXRNwhYOMMAcS2JedrrocPb5J37Oi/V0Z
t1xFxkuKNMYywJhqumKDvF9GLEoJ3e5SybDNWLG6/VPtxG1ehvvhd00Nu+fYloJy
yurSPTsjQn+Ck35nJlTxPCwDp4NHgCccfhPsuZ2K6fLiAV3Qk7buGcLhpnu4KZdD
M/7Kwk/7Qsd2S74dlxFWMw/qKPvbAqTVdshdeJQppJEt1E5IftKU48QWsYiqWVzi
1oK49AUCgYEA+oB9SiD/3nwhJdGw4eTFyXjdcSdycNYgqDDiGTbsAfbbGfpoRyQK
sP+GGueiaxYrTG1oX/O8OpNfcB/V8LJXCAWxPCC9IGyFHJY51QY6GEfmdGPGg83B
THx7ndFamuUCXYGgfm1a4sDVzEDXppcFhq5e1aYMt7vjKiyUIlRVw0sCgYEA0kXL
pDQbJUEtfKK7aXgQvHW9c7gGYk2uO0sHX0XmSCRufTaPthplH12vEoGdodRytKrP
LLAKsNag5w3Pqfe8j366/2/dVni7ZZ0EgtHEzI3Nzsu9Bv/Cod17VsRqJJhGT1sO
UYYiz59V/nHy62LznzmarrRvHZUvIf5294/3wzMCgYBg8OyLjW23me3yBs4e/TuV
llKMAaoGBLeqnxBAlcKLmA5+ZSuJrGb+zEQHkXCHZJLpBsYHq/lB6dpbN9bBSSLp
Fdra3AiUzapNv80tpTVCREDo9dvTEgMmKF5mWqhnnQxnR0zm4Eb1zY5vdwB9laAr
2LsWjyYIzItCvPGuv1hZNQKBgQCVecA1zkStyxOG0jff1dYv4pL2vA7tusLsq31m
IM5+4kcZA7xNe53/Gh3mTfW8oMWZ4QK9hbdT1n8zQQIqlU2ZxEEybFcvrUuqBGC8
jloT/4oaQ4LwgsfdQRD4sU+zC/yPVO9szKnaZi4/XsmMwx7kRZLMNr/2i6Khy77O
Za1/OQKBgB8yuUEJlPil4pRgi8YKfAJtl/2ZqnUM/reMCiabtdfug2IKn9MPzJmQ
qHE342nWPr1NmNsvzA1cu0pEdDcdljw9iA0BI1XFxxXhoMlCzcp5MUXSzTzQLSsy
8Gpek/FPpYwPp0g/XJPu5TaZACAHsF0DM+2fM3UKkGcUMMjCGrIk
-----END RSA PRIVATE KEY-----
path: /var/ssh/aavn-ict/
owner: root:root
permissions: '0644'
runcmd:
- 'sudo yum install epel-release -y'
- 'sudo yum check update -y'
- 'sudo yum install ansible htop -y'