#cloud-config
manage_etc_hosts: true
locale: "en_US.UTF-8"
fqdn: ${instance_name}-${client}-${service}-${env}-${prov-reg}.${domain}
system_info:
  default_user:
   name: cloud-user
chpasswd:
  list: |
    root:s3cr3t
  expire: False
disable_root: True
manage-resolv-conf: true
resolv_conf:
  nameservers: ['8.8.4.4', '8.8.8.8']
  options:
    rotate: true
    timeout: 1
#package_update: true
#package_upgrade: true
growpart:
  mode: auto
  devices: ['/']
  ignore_growroot_disabled: false
yum_repos:
  saltrepo:
    baseurl: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/${salt_minion_version}
    enabled: true
    failovermethod: priority
    gpgcheck: true
    gpgkey: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/${salt_minion_version}/SALTSTACK-GPG-KEY.pub
    name: Salt
packages:
 - salt-minion
runcmd:
 - 'cp /etc/skel/.bashrc /root/.bashrc'
 - 'sed -i -e "/^#master: salt/s/^.*\$/master: ${salt_master_fqdn}/" /etc/salt/minion'
 - 'systemctl enable salt-minion.service'
 - 'systemctl restart salt-minion.service'
write_files:
 - content: |
        alias ls='ls --color=auto'
        alias grep='grep --color=auto'
        N="\[\e[0m\]"
        R="\[\e[1;31m\]"
        G="\[\033[32m\]"
        if [[ $EUID == 0 ]] ; then
          PS1="$R\u@\h :[ $N\w$R ]:# $N"
        else
          PS1="$G\u@\h :[ $N\w$G ]:\$ $N"
        fi
        unset R G N
#
   path: /etc/skel/.bashrc
   owner: root:root
   permissions: '0644'