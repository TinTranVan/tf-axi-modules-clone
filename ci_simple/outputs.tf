/************************************************
* Instace outputs
***********************************************/
output "instance-name" {
  value = "${cloudstack_instance.ci_simple.name}"
}

output "instance-ip" {
  value = "${cloudstack_instance.ci_simple.ip_address}"
}

output "instance-id" {
  value = "${cloudstack_instance.ci_simple.id}"
}