/************************************************
* General info
***********************************************/
variable "cs_zone"     {
  description = "Cloudstack zone to be used"
}

/************************************************
* Instance info
***********************************************/

variable "service_offering" {
  description = "Instance service offering to use"
  default = "2vCPU-4GB-SSD-STD-SF"
}

variable "instance_template" {
  description = "Instance template to use"
}

variable "root_disk_size" {
  description = "Set the root disk size in GB"
}

variable "instance_name" {
  description = "Instance name"
}

variable "ssh_keypair" {
  description = "The name of the SSH key pair to access this instance"
}

variable "host_ssh_private" {
  description = "(Optional) The name of the SSH private key for the host"
  default = "none"
}

variable "network_id" {
  description = "Cloudstack hosting network id for instance"
}

variable "ip_address" {
  description = "Cloudstack static ip address for instance"
  default = ""
}

variable "domain_name_servers" {
  description = "String of comma seperated domain name servers to use in DHCP option sets"
  default = "8.8.8.8,8.8.4.4"
}

/************************************************
* Userdata variables
***********************************************/

variable "user-data" {
  description = "The user data file name to be used"
  default = "userdata.tpl"
}

variable "service" {
  description = "Most granular of taxonomy. e.g. `promise`, `dataproc`, `cassandra`, `mgmt`, etc"
}

variable "client" {
  description = "A business entity/client of devops. e.g. `mobiliar`, `suva`, `axi`, `ops`"
}

variable "env" {
  description = "Short for environment. e.g. `dev`, `test`, `stg`, `prod`"
}

variable "prov-reg" {
  description = "Region and cloud provider identifier e.g `s1s` or `s2s` for Swiss1/Swiss2 of SSC"
}

variable "domain" {
  description = "The domain used for the instance"
}

variable "salt_minion_version" {
  description = "Salt minion version to install"
}

variable "salt_master_fqdn" {
  description = "Salt master FQDN or IP"
}
