/************************************************
* Instance resource for module
***********************************************/
resource "cloudstack_instance" "ci_simple" {
  name                = "${format("tf-%v-%v-%v-%v", var.instance_name, var.service, var.client, var.env)}"
  service_offering    = "${var.service_offering}"
  template            = "${var.instance_template}"
  network_id          = "${var.network_id}"
  ip_address          = "${var.ip_address}"
  zone                = "${var.cs_zone}"
  root_disk_size      = "${var.root_disk_size}"
  user_data           = "${data.template_cloudinit_config.config.rendered}"
  keypair             = "${var.ssh_keypair}"
  expunge             = true
}