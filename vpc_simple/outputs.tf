/************************************************
* VPC outputs
***********************************************/
output "vpc_main_id" {
  value = "${cloudstack_vpc.main.*.id}"
}

output "vpc_main_name" {
  value = "${cloudstack_vpc.main.*.name}"
}

output "vpc_main_cidr" {
  value = "${cloudstack_vpc.main.*.cidr}"
}

output "vpc_source_nat_ip" {
  value = "${cloudstack_vpc.main.*.source_nat_ip}"
}

/************************************************
* Subnet outputs
***********************************************/

output "subnet_app_id" {
  value = "${cloudstack_network.app.*.id}"
}

output "subnet_app_cidr" {
  value = "${cloudstack_network.app.*.cidr}"
}

output "subnet_bkd_id" {
  value = "${cloudstack_network.bkd.*.id}"
}

output "subnet_bkd_cidr" {
  value = "${cloudstack_network.bkd.*.cidr}"
}

output "subnet_db_id" {
  value = "${cloudstack_network.db.*.id}"
}

output "subnet_db_cidr" {
  value = "${cloudstack_network.db.*.cidr}"
}

output "subnet_dmz_id" {
  value = "${cloudstack_network.dmz.*.id}"
}

output "subnet_dmz_cidr" {
  value = "${cloudstack_network.dmz.*.cidr}"
}

output "subnet_web_id" {
  value = "${cloudstack_network.web.*.id}"
}

output "subnet_web_cidr" {
  value = "${cloudstack_network.web.*.cidr}"
}

/************************************************
* ACL outputs
***********************************************/

output "subnet_acl_app_id" {
  value = "${cloudstack_network_acl.app-acl.*.id}"
}

output "subnet_acl_bkd_id" {
  value = "${cloudstack_network_acl.bkd-acl.*.id}"
}

output "subnet_acl_db_id" {
  value = "${cloudstack_network_acl.db-acl.*.id}"
}

output "subnet_acl_dmz_id" {
  value = "${cloudstack_network_acl.dmz-acl.*.id}"
}

output "subnet_acl_web_id" {
  value = "${cloudstack_network_acl.web-acl.*.id}"
}

