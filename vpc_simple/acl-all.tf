resource "cloudstack_network_acl" "web-acl" {
    count = "${var.create_web_net}"
    depends_on = ["cloudstack_vpc.main"]
    name = "web-acl"
    vpc_id = "${cloudstack_vpc.main.id}"
}

resource "cloudstack_network_acl" "app-acl" {
    count = "${var.create_app_net}"
    depends_on = ["cloudstack_vpc.main"]
    name = "app-acl"
    vpc_id = "${cloudstack_vpc.main.id}"
}


resource "cloudstack_network_acl" "db-acl" {
    count = "${var.create_db_net}"
    depends_on = ["cloudstack_vpc.main"]
    name = "db-acl"
    vpc_id = "${cloudstack_vpc.main.id}"
}

resource "cloudstack_network_acl" "bkd-acl" {
    count = "${var.create_bkd_net}"
    depends_on = ["cloudstack_vpc.main"]
    name = "bkd-acl"
    vpc_id = "${cloudstack_vpc.main.id}"
}

resource "cloudstack_network_acl" "dmz-acl" {
    count = "${var.create_dmz_net}"
    depends_on = ["cloudstack_vpc.main"]
    name = "dmz-acl"
    vpc_id = "${cloudstack_vpc.main.id}"
}