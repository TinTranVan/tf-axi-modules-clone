resource "cloudstack_network" "dmz" {
    count = "${var.create_dmz_net}"
    name = "${format("subnet-dmz-%v-%v", var.client ,var.env)}"
    display_text = "${format("subnet-dmz-%v-%v", var.client ,var.env)}"
    cidr = "${cidrsubnet(var.vpc_cidr,8,50)}"
    network_offering = "${var.subnet_offering}"
    acl_id = "${cloudstack_network_acl.dmz-acl.id}"
    zone = "${var.cs_zone}"
    vpc_id = "${cloudstack_vpc.main.id}"
}
