/************************************************
* General info
***********************************************/
variable "cs_zone"     {
  description = "Cloudstack zone to be used"
}

/************************************************
* VPC info
***********************************************/
variable "vpc_offering" {
   description = "VPC offering to use"
   default = "Default VPC offering 100Mbps"
}

variable "vpc_cidr" {
  description = "CIDR for VPC"
}

variable "domain_name_servers" {
  description = "String of comma seperated domain name servers to use in DHCP option sets"
  default = "8.8.8.8,8.8.4.4"
}

/************************************************
* Subnets and ACLs
***********************************************/
variable "subnet_offering" {
  description = "VPC network offering to use"
  default = "Offering for Isolated VPC networks - without LoadBalancer"
}

variable "create_app_net" {
  description = "If set to true, create a app network in VPC"
}

variable "create_bkd_net" {
  description = "If set to true, create a bkd network in VPC"
}

variable "create_db_net" {
  description = "If set to true, create a db network in VPC"
}

variable "create_dmz_net" {
  description = "If set to true, create a dmz network in VPC"
}

variable "create_web_net" {
  description = "If set to true, create a web network in VPC"
}

/************************************************
* Userdata variables
***********************************************/
variable "service" {
  description = "Most granular of taxonomy. e.g. `promise`, `dataproc`, `cassandra`, etc"
}

variable "client" {
  description = "A business entity/client of devops. e.g. `mobiliar`, `suva`, `axi`, `mgmt`"
}

variable "env" {
  description = "Short for environment. e.g. `dev`, `test`, `stg`, `prod`"
}
