resource "cloudstack_network" "db" {
    count = "${var.create_db_net}"
    name = "${format("subnet-db-%v-%v", var.client ,var.env)}"
    display_text = "${format("subnet-db-%v-%v", var.client ,var.env)}"
    cidr = "${cidrsubnet(var.vpc_cidr,8,30)}"
    network_offering = "${var.subnet_offering}"
    acl_id = "${cloudstack_network_acl.db-acl.id}"
    zone = "${var.cs_zone}"
    vpc_id = "${cloudstack_vpc.main.id}"
}
