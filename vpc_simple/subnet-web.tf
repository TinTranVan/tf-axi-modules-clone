resource "cloudstack_network" "web" {
    count = "${var.create_web_net}"
    name = "${format("subnet-web-%v-%v", var.client ,var.env)}"
    display_text = "${format("subnet-web-%v-%v", var.client ,var.env)}"
    cidr = "${cidrsubnet(var.vpc_cidr,8,20)}"
    network_offering = "${var.subnet_offering}"
    acl_id = "${cloudstack_network_acl.web-acl.id}"
    zone = "${var.cs_zone}"
    vpc_id = "${cloudstack_vpc.main.id}"
}
