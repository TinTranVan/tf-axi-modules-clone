resource "cloudstack_network" "bkd" {
    count = "${var.create_bkd_net}"
    name = "${format("subnet-bkd-%v-%v", var.client ,var.env)}"
    display_text = "${format("subnet-bkd-%v-%v", var.client ,var.env)}"
    cidr = "${cidrsubnet(var.vpc_cidr,8,40)}"
    network_offering = "${var.subnet_offering}"
    acl_id = "${cloudstack_network_acl.bkd-acl.id}"
    zone = "${var.cs_zone}"
    vpc_id = "${cloudstack_vpc.main.id}"
}
