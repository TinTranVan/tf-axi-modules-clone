resource "cloudstack_network" "app" {
    count = "${var.create_app_net}"
    name = "${format("subnet-app-%v-%v", var.client ,var.env)}"
    display_text = "${format("subnet-app-%v-%v", var.client ,var.env)}"
    cidr = "${cidrsubnet(var.vpc_cidr,8,10)}"
    network_offering = "${var.subnet_offering}"
    acl_id = "${cloudstack_network_acl.app-acl.id}"
    zone = "${var.cs_zone}"
    vpc_id = "${cloudstack_vpc.main.id}"
}
