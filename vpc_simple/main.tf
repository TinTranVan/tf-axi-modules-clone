resource "cloudstack_vpc" "main" {
    name = "${format("tf-vpc-%v-%v-%v", var.service, var.client, var.env)}"
    display_text = "${format("tf-vpc-%v-%v-%v", var.service, var.client, var.env)}"
    cidr = "${var.vpc_cidr}"
    vpc_offering = "${var.vpc_offering}"
    zone = "${var.cs_zone}"
}
